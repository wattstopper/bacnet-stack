/**
 * @file
 * @author Steve Karg
 * @date 2015
 * @brief Millisecond timer library HAL implementation - Contiki
 *
 * @section DESCRIPTION
 *
 * The mstimer library provides functions for setting, resetting and
 * restarting timers, and for checking if a timer has expired. An
 * application must "manually" check if its timers have expired; this
 * is not done automatically.
 *
 * A timer is declared as a \c struct \c mstimer and all access to the
 * timer is made by a pointer to the declared timer.
 *
 * Adapted from the Contiki operating system.
 * Original Authors: Adam Dunkels <adam@sics.se>, Nicolas Tsiftes <nvt@sics.se>
 */
#include <stdbool.h>
#include "sys/clock.h"
#include "sys/cc.h"
#include "sys/rtimer.h"
#include "mstimer.h"

/* configuration clocks */
static struct rtimer ms_clock;
#define RTIMER_MILLISECOND (RTIMER_SECOND / 1000)
static volatile unsigned long Millisecond_Counter;
static bool MSTimer_Initialized;

/**
 * @brief Function retrieves the system time, in milliseconds.
 * The system time is the time elapsed since OS was started.
 * @return milliseconds since OS was started
 */
unsigned long mstimer_now(void)
{
    return Millisecond_Counter;
}

/**
 * @brief Callback from rtimer: Increment millisecond timer; reload rtimer
 *
 * @param t - rtimer data that is us
 * @param ptr - context data from the callback
 */
static void rt_do_mstimer(struct rtimer *t, void *ptr)
{
    rtimer_set(t, RTIMER_TIME(t) + RTIMER_MILLISECOND, 1,
        (rtimer_callback_t)rt_do_mstimer, ptr);
    Millisecond_Counter++;
}

/**
 * @brief Initialization for timer
 */
void mstimer_init(void)
{
    int status;

    if (!MSTimer_Initialized) {
        status = rtimer_set(&ms_clock, RTIMER_NOW() + RTIMER_MILLISECOND, 1,
            (rtimer_callback_t)rt_do_mstimer, NULL);
        if (status == RTIMER_OK) {
            MSTimer_Initialized = true;
        }
    }
}