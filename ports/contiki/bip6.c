/*####COPYRIGHTBEGIN####
 -------------------------------------------
 Copyright (C) 2016 Steve Karg

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to:
 The Free Software Foundation, Inc.
 59 Temple Place - Suite 330
 Boston, MA  02111-1307, USA.

 As a special exception, if other files instantiate templates or
 use macros or inline functions from this file, or you compile
 this file and link it with other works to produce a work based
 on this file, this file does not by itself cause the resulting
 work to be covered by the GNU General Public License. However
 the source code for this file must still be made available in
 accordance with section (3) of the GNU General Public License.

 This exception does not invalidate any other reasons why a work
 based on this file might be covered by the GNU General Public
 License.
 -------------------------------------------
####COPYRIGHTEND####*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h> /* for standard integer types uint8_t etc. */
#include <stdbool.h> /* for the standard bool type. */
#include "bacdcode.h"
#include "config.h"
#include "bip6.h"
#include "bvlc6.h"
#include "device.h"
#include "handlers.h"
#include "mstimer.h"
#include "ringbuf.h"
/* contiki specific includes */
#include "contiki.h"
#include "contiki-net.h"
#include "net/ip/uip.h"
#include "lib/random.h"
#include "net/ip/uip.h"
#include "net/ipv6/uip-ds6.h"
#include "net/ip/simple-udp.h"
#include "net/ipv6/sicslowpan.h"

#ifndef BIP6_DEBUG_ENABLED
#define BIP6_DEBUG_ENABLED 0
#endif
#define DEBUG BIP6_DEBUG_ENABLED
#include "net/ip/uip-debug.h"
#if DEBUG
#warning "BIP6: debug enabled!"
#endif

/** @file contiki/bip6.c  Initializes BACnet/IPv6 interface (Contiki). */

static bool bip6_transmit_fsm(void);
/* local address - filled by init functions */
static BACNET_IP6_ADDRESS BIP6_Addr;
static BACNET_IP6_ADDRESS BIP6_Broadcast_Addr;
#ifndef DEFAULT_BIP6_PORT
#define DEFAULT_BIP6_PORT 0xBAC0
#endif
/* data structure for transmit packet */
struct bip6_tx_packet {
    BACNET_IP6_ADDRESS dest;
    uint8_t mtu[MAX_MPDU];
    uint16_t mtu_len;
    uint8_t retry_count;
};

// max number of repeats
#ifndef MAX_BACNET_UDP_REPEAT
#define MAX_BACNET_UDP_REPEAT 4
#endif
// max random delay
#ifndef MAX_BACNET_PACKET_DELAY
#define MAX_BACNET_PACKET_DELAY 32 // 128 discover irb seams to fail
#endif
// min transmit delay
#ifndef CONST_BACNET_PACKET_DELAY
#define CONST_BACNET_PACKET_DELAY 0
#endif

// number of packet repeats to allow
#ifndef BIP6_MCAST_REPEAT_FILTER_PACKETS
#define BIP6_MCAST_REPEAT_FILTER_PACKETS 2
#endif
#ifndef BIP6_MCAST_FILTER_SECONDS
#define BIP6_MCAST_FILTER_SECONDS 1
#endif
static uint8_t Multicast_Repeat_Count = BIP6_MCAST_REPEAT_FILTER_PACKETS;
static struct ctimer Multicast_Timer;
static uint8_t Multicast_Repeat_Buffer[MAX_MPDU];

#define UIP_IP_BUF ((struct uip_ip_hdr *)&uip_buf[UIP_LLH_LEN])
#define UIP_UDP_BUF ((struct uip_udp_hdr *)&uip_buf[uip_l2_l3_hdr_len])

/* count must be a power of 2 for ringbuf library */
#ifndef BIP6_TRANSMIT_PACKET_COUNT
#define BIP6_TRANSMIT_PACKET_COUNT 16
#endif
static struct bip6_tx_packet Transmit_Buffer[BIP6_TRANSMIT_PACKET_COUNT];
static RING_BUFFER Transmit_Queue;
/* force bypass of the random delays */
static const bool Bypass_Delay = true;
/* state to handle orderly transitions while waiting */
static enum {
    BIP_TX_STATE_IDLE,
    BIP_TX_STATE_WAIT,
    BIP_TX_STATE_SEND
} BIP6_Tx_State = BIP_TX_STATE_IDLE;
/* packet that is transmitting */
static struct bip6_tx_packet *BIP6_Tx_Pkt;
/* amount of time to wait before transmitting */
static struct mstimer BIP6_Transmit_Delay_Timer;

/* UDP and IPv6 parameters */
static struct uip_udp_conn *udp_conn_bacnet;

/**
 * Set the BACnet IPv6 UDP port number
 *
 * @param port - IPv6 UDP port number
 */
void bip6_set_port(uint16_t port)
{
    BIP6_Addr.port = port;
    BIP6_Broadcast_Addr.port = port;
    PRINTF("BIP6: IPv6 bip6_set_port: %u\n", BIP6_Addr.port);
}

/**
 * Get the BACnet IPv6 UDP port number
 *
 * @return IPv6 UDP port number
 */
uint16_t bip6_get_port(void)
{
    return BIP6_Addr.port;
}

/* global uIPv6 Interface */
extern uip_ds6_netif_t uip_ds6_if;

/**
 * Set the interface name or IP address
 *
 * @param ifname - C string for name or text address
 */
void bip6_set_interface(char *ifname)
{
    uint8_t i;
    for (i = 0; i < UIP_DS6_ADDR_NB; i++) {
        if (uip_ds6_if.addr_list[i].isused) {
            bvlc6_address_set(&BIP6_Addr,
                uip_ntohs(uip_ds6_if.addr_list[i].ipaddr.u16[0]),
                uip_ntohs(uip_ds6_if.addr_list[i].ipaddr.u16[1]),
                uip_ntohs(uip_ds6_if.addr_list[i].ipaddr.u16[2]),
                uip_ntohs(uip_ds6_if.addr_list[i].ipaddr.u16[3]),
                uip_ntohs(uip_ds6_if.addr_list[i].ipaddr.u16[4]),
                uip_ntohs(uip_ds6_if.addr_list[i].ipaddr.u16[5]),
                uip_ntohs(uip_ds6_if.addr_list[i].ipaddr.u16[6]),
                uip_ntohs(uip_ds6_if.addr_list[i].ipaddr.u16[7]));
        }
    }
}

/**
 * Get the IPv6 broadcast address for my interface.
 * Used as dest address in messages sent as BROADCAST
 *
 * @param addr - IPv6 source address
 */
void bip6_get_broadcast_address(BACNET_ADDRESS *addr)
{
    if (addr) {
        addr->net = BACNET_BROADCAST_NETWORK;
        addr->mac_len = 0;
        addr->len = 0;
    }
}

/**
 * Get the IPv6 address for my interface.
 * Used as VMAC src address in messages sent.
 *
 * @param addr - IPv6 source address
 */
void bip6_get_my_address(BACNET_ADDRESS *addr)
{
    uint32_t device_id = 0;

    if (addr) {
        device_id = Device_Object_Instance_Number();
        bvlc6_vmac_address_set(addr, device_id);
    }
}

/**
 * Set the BACnet/IP address
 *
 * @param addr - network IPv6 address
 */
bool bip6_set_addr(BACNET_IP6_ADDRESS *addr)
{
    return bvlc6_address_copy(&BIP6_Addr, addr);
}

/**
 * Get the BACnet/IP address
 *
 * @return BACnet/IP address
 */
bool bip6_get_addr(BACNET_IP6_ADDRESS *addr)
{
    return bvlc6_address_copy(addr, &BIP6_Addr);
}

/**
 * Determine if the BACnet/IPv6 address matches our own address
 *
 * @param addr - network IPv6 address
 *
 * @return true if the BACnet/IPv6 address matches our own address
 */
bool bip6_address_match_self(BACNET_IP6_ADDRESS *addr)
{
    BACNET_IP6_ADDRESS *my_addr = &BIP6_Addr;
    bool mismatch = false;
    uint8_t i;

    for (i = IP6_ADDRESS_MAX / 2; i < IP6_ADDRESS_MAX; i++) {
        if (my_addr->address[i] != addr->address[i]) {
            mismatch = true;
        }
    }
    if (my_addr->port != addr->port) {
        mismatch = true;
    }

    return !mismatch;
}

/**
 * Set the BACnet/IP address
 *
 * @param addr - network IPv6 address
 *
 * @return true if the set was successful
 */
bool bip6_set_broadcast_addr(BACNET_IP6_ADDRESS *addr)
{
    return bvlc6_address_copy(&BIP6_Broadcast_Addr, addr);
}

/**
 * Get the BACnet/IP address
 *
 * @return true if the set was successful
 */
bool bip6_get_broadcast_addr(BACNET_IP6_ADDRESS *addr)
{
    return bvlc6_address_copy(addr, &BIP6_Broadcast_Addr);
}

/**
 * Copy uIP IPv6 address to the BACnet IPv6 address
 *
 * @return true if the copy was successful
 */
bool bip6_address_copy_from_uip(BACNET_IP6_ADDRESS *dst, uip_ip6addr_t *src)
{
    bool status = false;
    unsigned int i = 0;

    if (src && dst) {
        for (i = 0; i < IP6_ADDRESS_MAX; i++) {
            dst->address[i] = src->u8[i];
        }
        status = true;
    }

    return status;
}

/**
 * Copy uIP IPv6 address to the BACnet IPv6 address
 *
 * @return true if the copy was successful
 */
bool bip6_address_copy_to_uip(uip_ip6addr_t *dst, BACNET_IP6_ADDRESS *src)
{
    bool status = false;
    unsigned int i = 0;

    if (src && dst) {
        for (i = 0; i < IP6_ADDRESS_MAX; i++) {
            src->address[i] = dst->u8[i];
        }
        status = true;
    }

    return status;
}

/**
 * The send function for BACnet/IPv6 driver layer
 *
 * @param dest - Points to a BACNET_IP6_ADDRESS structure containing the
 *  destination address.
 * @param mtu - the bytes of data to send
 * @param mtu_len - the number of bytes of data to send
 *
 * @return Upon successful completion, returns the number of bytes sent.
 *  Otherwise, 0 shall be returned.
 */
static uint16_t bip6_send_mpdu_local(
    BACNET_IP6_ADDRESS *dest, uint8_t *mtu, uint16_t mtu_len)
{
    uint16_t bytes_sent = 0;
    uint16_t addr16[8];

    /* Configure connection */
    bvlc6_address_get(dest, &addr16[0], &addr16[1], &addr16[2], &addr16[3],
        &addr16[4], &addr16[5], &addr16[6], &addr16[7]);
    udp_conn_bacnet->ripaddr.u16[0] = uip_ntohs(addr16[0]);
    udp_conn_bacnet->ripaddr.u16[1] = uip_ntohs(addr16[1]);
    udp_conn_bacnet->ripaddr.u16[2] = uip_ntohs(addr16[2]);
    udp_conn_bacnet->ripaddr.u16[3] = uip_ntohs(addr16[3]);
    udp_conn_bacnet->ripaddr.u16[4] = uip_ntohs(addr16[4]);
    udp_conn_bacnet->ripaddr.u16[5] = uip_ntohs(addr16[5]);
    udp_conn_bacnet->ripaddr.u16[6] = uip_ntohs(addr16[6]);
    udp_conn_bacnet->ripaddr.u16[7] = uip_ntohs(addr16[7]);
    udp_conn_bacnet->rport = uip_ntohs(dest->port);
    if (uip_udp_packet_send(udp_conn_bacnet, mtu, mtu_len)) {
        PRINTF("BIP6: sent UDP datagram (%u bytes)\n", mtu_len);
        bytes_sent = mtu_len;
    }
    /* Restore server connection to allow data from any node */
    memset(&udp_conn_bacnet->ripaddr, 0, sizeof(udp_conn_bacnet->ripaddr));
    udp_conn_bacnet->rport = 0;

    return bytes_sent;
}

/**
 * The common send function for BACnet/IPv6 application layer
 *
 * @param dest - Points to a #BACNET_ADDRESS structure containing the
 *  destination address.
 * @param npdu_data - Points to a BACNET_NPDU_DATA structure containing the
 *  destination network layer control flags and data.
 * @param pdu - the bytes of data to send
 * @param pdu_len - the number of bytes of data to send
 * @return Upon successful completion, returns the number of bytes sent.
 *  Otherwise, -1 shall be returned and errno set to indicate the error.
 */
int bip6_send_pdu(BACNET_ADDRESS *dest,
    BACNET_NPDU_DATA *npdu_data,
    uint8_t *pdu,
    unsigned pdu_len)
{
    return bvlc6_send_pdu(dest, npdu_data, pdu, pdu_len);
}

/**
 * The receive function for BACnet/IPv6 driver layer
 * Contiki uses a call back so we no longer need to check for a packet here
 * But this function drives the statemachine, so it must be called
 */

uint16_t bip6_receive(
    BACNET_ADDRESS *src, uint8_t *npdu, uint16_t max_npdu, unsigned timeout)
{
    // use this to limit how fast we processes packets?
    if (bip6_transmit_fsm()) {
        return 0;
    }
    return 0;
}

/**
 * The send function for BACnet/IPv6 driver layer
 *
 * @param dest - Points to a BACNET_IP6_ADDRESS structure containing the
 *  destination address.
 * @param mtu - the bytes of data to send
 * @param mtu_len - the number of bytes of data to send
 *
 * @return Upon successful completion, returns the number of bytes sent.
 *  Otherwise, 0 shall be returned.
 */
int bip6_send_mpdu(BACNET_IP6_ADDRESS *dest, uint8_t *mtu, uint16_t mtu_len)
{
    struct bip6_tx_packet *pkt;
    int byte_count = 0;
    if (mtu_len < MAX_MPDU) {
        pkt = (struct bip6_tx_packet *)Ringbuf_Data_Peek(&Transmit_Queue);
        if (pkt) {
            memcpy(&pkt->dest, dest, sizeof(BACNET_IP6_ADDRESS));
            memcpy(&pkt->mtu, mtu, mtu_len);
            pkt->mtu_len = mtu_len;
            pkt->retry_count = 0;
            if (Ringbuf_Data_Put(&Transmit_Queue, (uint8_t *)pkt)) {
                byte_count = mtu_len;
            }
            PRINTF("BIP6: TX FSM Buffer Added (%d)\n",
                Ringbuf_Count(&Transmit_Queue));
        } else {
            PRINTF("BIP6: TX FSM Buffer Full! (%d)\n",
                Ringbuf_Count(&Transmit_Queue));
        }
    }

    return byte_count;
}

/**
 * Transmit Packet Finite State Machine - handles a random
 * delayed send, along with limited retries upon failure.
 *
 * @return true if the packet is transmitting
 */
static bool bip6_transmit_fsm(void)
{
    uint32_t milliseconds = 0;

    switch (BIP6_Tx_State) {
        case BIP_TX_STATE_IDLE:
            /* get the packet - but don't remove it from queue */
            BIP6_Tx_Pkt =
                (struct bip6_tx_packet *)Ringbuf_Peek(&Transmit_Queue);
            if (BIP6_Tx_Pkt) {
                if (Bypass_Delay) {
                    BIP6_Tx_State = BIP_TX_STATE_SEND;
                    PRINTF("BIP6: TX FSM Bypass Delay\n");
                    mstimer_set(&BIP6_Transmit_Delay_Timer, milliseconds);
                } else if (Ringbuf_Full(&Transmit_Queue)) {
                    PRINTF("BIP6: TX FSM Buffer Full - Bypass Delay\n");
                    BIP6_Tx_State = BIP_TX_STATE_SEND;
                    mstimer_set(&BIP6_Transmit_Delay_Timer, milliseconds);
                } else {
                    milliseconds = CONST_BACNET_PACKET_DELAY +
                        ((uint32_t)(rand()) % MAX_BACNET_PACKET_DELAY);
                    mstimer_set(&BIP6_Transmit_Delay_Timer, milliseconds);
                    PRINTF("BIP6: TX FSM delay=%ums\n", (unsigned)milliseconds);
                    BIP6_Tx_State = BIP_TX_STATE_WAIT;
                }
            }
            break;
        case BIP_TX_STATE_WAIT:
            PRINTF("BIP6: TX FSM waiting\n");
            if (mstimer_interval(&BIP6_Transmit_Delay_Timer)) {
                if (mstimer_expired(&BIP6_Transmit_Delay_Timer)) {
                    BIP6_Tx_State = BIP_TX_STATE_SEND;
                }
            } else {
                BIP6_Tx_State = BIP_TX_STATE_IDLE;
            }
            break;
        case BIP_TX_STATE_SEND:
            PRINTF("BIP6: TX FSM sending\n");
            if (BIP6_Tx_Pkt) {
                if (bip6_send_mpdu_local(&BIP6_Tx_Pkt->dest, BIP6_Tx_Pkt->mtu,
                        BIP6_Tx_Pkt->mtu_len) > 0) {
                    /* success! remove the packet from the queue */
                    (void)Ringbuf_Pop(&Transmit_Queue, NULL);
                } else {
                    BIP6_Tx_Pkt->retry_count++;
                    PRINTF("BIP6: TX FSM retry %d\n", BIP6_Tx_Pkt->retry_count);
                    /* collision or timeout */
                    if (BIP6_Tx_Pkt->retry_count > MAX_BACNET_UDP_REPEAT) {
                        /* remove the packet from the queue */
                        (void)Ringbuf_Pop(&Transmit_Queue, NULL);
                    }
                }
            }
            BIP6_Tx_State = BIP_TX_STATE_IDLE;
            break;
        default:
            BIP6_Tx_State = BIP_TX_STATE_IDLE;
            break;
    }

    return (BIP6_Tx_State != BIP_TX_STATE_IDLE);
}

/**
 * Determine if the datalink transmit queue is empty.
 * Used for pacing the application layer.
 *
 * @return true if the transmit queue is empty
 */
bool bip6_send_pdu_queue_empty(void)
{
    return Ringbuf_Empty(&Transmit_Queue);
}

/**
 * callback for multicast repeat expiration
 */
static void mcast_expired(void *passed_data)
{
    ctimer_stop(&Multicast_Timer);
    Multicast_Repeat_Count = BIP6_MCAST_REPEAT_FILTER_PACKETS;
}

/**
 * callback for bacnet packet recive
 */
void bip6_receive_callback(void)
{
    BACNET_ADDRESS src;
    uint8_t *npdu;

    uint16_t npdu_len = 0; /* return value */
    BACNET_IP6_ADDRESS addr = { { 0 } };
    int received_bytes = 0;
    int offset = 0;
    uint16_t i = 0;
    uint16_t dupdata = 1;

    ANNOTATE("BIP6: received UDP datagram! new_data=%s\n",
        uip_newdata() ? "yes" : "no");
    if (uip_newdata()) {
        // PRINTF("r->\n");
        PRINTF("BIP6: receiving UDP datagram from: ");
        PRINT6ADDR(&UIP_IP_BUF->srcipaddr);
        PRINTF(":%u len=%u\n", uip_ntohs(UIP_UDP_BUF->srcport), uip_datalen());
        received_bytes = uip_datalen();
        npdu = (uint8_t *)uip_appdata;
        npdu[received_bytes] = 0; // null terminate

        /* filter that only passes unique packets
            to the application code, no duplicates */
        if (Multicast_Repeat_Buffer == NULL || npdu == NULL ||
            received_bytes >= MAX_MPDU) {
            PRINTF("BIP6: multicast buffer=%p npdu=%p len=%d\n",
                Multicast_Repeat_Buffer, npdu, received_bytes);
        } else {
            dupdata = memcmp(Multicast_Repeat_Buffer, npdu, received_bytes);
        }
        if ((Multicast_Repeat_Count == 0) && (dupdata == 0)) {
            ANNOTATE("BIP6: repeated multicast - dropped.\n");
            return;
        } else {
            memcpy(Multicast_Repeat_Buffer, npdu, received_bytes);
            ctimer_set(&Multicast_Timer,
                CLOCK_SECOND * BIP6_MCAST_FILTER_SECONDS, mcast_expired,
                (void *)NULL);
            if (Multicast_Repeat_Count > 0) {
                Multicast_Repeat_Count--;
            }
        }
        /* the signature of a BACnet/IPv6 packet */
        if (npdu[0] != BVLL_TYPE_BACNET_IP6) {
            ANNOTATE("BIP6: not BACnet/IPv6 packet - dropped.\n");
            return;
        }
        /* pass the packet into the BBMD handler */
        memcpy(addr.address, (&UIP_IP_BUF->srcipaddr), 16);
        addr.port = uip_ntohs(UIP_UDP_BUF->srcport);

        offset = bvlc6_handler(&addr, &src, npdu, received_bytes);
        if (offset > 0) {
            npdu_len = received_bytes - offset;
            if (npdu_len <= MAX_MPDU) {
                /* shift the buffer to return a valid NPDU */
                for (i = 0; i < npdu_len; i++) {
                    npdu[i] = npdu[offset + i];
                }
            } else {
                npdu_len = 0;
            }
        }
        if (npdu_len > 0) {
            npdu_handler(&src, &npdu[0], npdu_len);
        }
    }

    return;
}

/** Cleanup and close out the BACnet/IP services by closing the socket.
 * @ingroup DLBIP6
 */
void bip6_cleanup(void)
{
    // TODO is there anything that contiki does to close a socket?
    return;
}

/** Initialize the BACnet/IP services at the given interface.
 * @ingroup DLBIP6
 * -# Gets the local IP address and local broadcast address from the system,
 *  and saves it into the BACnet/IPv6 data structures.
 * -# Opens a UDP socket
 * -# Configures the socket for sending and receiving
 * -# Configures the socket so it can send multicasts
 * -# Binds the socket to the local IP address at the specified port for
 *    BACnet/IPv6 (by default, 0xBAC0 = 47808).
 *
 * @note For Linux, ifname is eth0, ath0, arc0, and others.
 *
 * @param ifname [in] The named interface to use for the network layer.
 *        If NULL, the "eth0" interface is assigned.
 * @return True if the socket is successfully opened for BACnet/IP,
 *         else False if the socket functions fail.
 */
bool bip6_init(char *ifname)
{
    int status = true;
    uip_ip6addr_t addr;
    uip_ds6_maddr_t *rv;

    Ringbuf_Init(&Transmit_Queue, (uint8_t *)Transmit_Buffer,
        sizeof(struct bip6_tx_packet), BIP6_TRANSMIT_PACKET_COUNT);
    if (BIP6_Addr.port == 0) {
        PRINTF("BIP6: PORT WAS 0\n");
        bip6_set_port(DEFAULT_BIP6_PORT);
    }
    bip6_set_interface(NULL);
    PRINTF("BIP6: IPv6 UDP port: %u\n", BIP6_Addr.port);
    if (BIP6_Broadcast_Addr.address[0] == 0) {
        bvlc6_address_set(&BIP6_Broadcast_Addr, BIP6_MULTICAST_SITE_LOCAL, 0, 0,
            0, 0, 0, 0, BIP6_MULTICAST_GROUP_ID);
    }
    PRINTF("BIP6: Prepare to join multicast group\n");
    uip_ip6addr(&addr, BIP6_MULTICAST_SITE_LOCAL, 0, 0, 0, 0, 0, 0,
        BIP6_MULTICAST_GROUP_ID);
    rv = uip_ds6_maddr_add(&addr);
    if (rv != NULL) {
        PRINTF("BIP6: Joined multicast group ");
        PRINT6ADDR(&uip_ds6_maddr_lookup(&addr)->ipaddr);
        PRINTF("\n");
    }
    bvlc6_init();
    /* new connection with remote host */
    udp_conn_bacnet = udp_new(NULL, uip_ntohs(0), NULL);
    if (udp_conn_bacnet != NULL) {
        udp_bind(udp_conn_bacnet, uip_htons(BIP6_Addr.port));
        PRINTF(
            "BIP6: Listening on port %u\n", uip_ntohs(udp_conn_bacnet->lport));
    }

    return status;
}
